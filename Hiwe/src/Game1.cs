﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using Hiwe.Model;

namespace Hiwe
{
    public class Game1 : Game
    {
        private GraphicsDeviceManager _graphics;
        private SpriteBatch _spriteBatch;

        private int width = 800;
        private int height = 600;

        private Texture2D hexagon_texture;

        private Dictionary<Bug.Type, Texture2D> bugTextures;
        private Dictionary<string, Texture2D> hexagonTextures;

        private ViewModel viewModel;

        private RenderTarget2D leftSide;
        private RenderTarget2D rightSide;


        private SpriteFont font;

        public Game1()
        {
            //first change in code
            _graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            IsMouseVisible = true;


            viewModel = new ViewModel();
        }

        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
            _graphics.PreferredBackBufferWidth = 800;
            _graphics.PreferredBackBufferHeight = 600;
            _graphics.ApplyChanges();

            font = Content.Load<SpriteFont>("defaultFont");
            leftSide = new RenderTarget2D(_graphics.GraphicsDevice, 200, 600);
            rightSide = new RenderTarget2D(_graphics.GraphicsDevice, 200, 600);
        }

        protected override void LoadContent()
        {
            _spriteBatch = new SpriteBatch(GraphicsDevice);

            //hexagon_texture = this.Content.Load<Texture2D>("hexagon_red");
            bugTextures = new Dictionary<Bug.Type, Texture2D>();
            bugTextures.Add(Bug.Type.ANT, this.Content.Load<Texture2D>("ant"));
            bugTextures.Add(Bug.Type.GRASSHOPPER, this.Content.Load<Texture2D>("grasshopper"));
            bugTextures.Add(Bug.Type.QUEEN, this.Content.Load<Texture2D>("queen"));
            bugTextures.Add(Bug.Type.SPIDER, this.Content.Load<Texture2D>("spider"));

            hexagonTextures = new Dictionary<string, Texture2D>();
            hexagonTextures.Add("red", this.Content.Load<Texture2D>("hexagon_red"));
            hexagonTextures.Add("blue", this.Content.Load<Texture2D>("hexagon_blue"));


            //    = this.Content.Load<Texture2D>("spider");



            // TODO: use this.Content to load your game content here
            /*
            hexagons = new Dictionary<Bug.Type, Texture2D>();

            
            hexagons.Add(Bug.Type.ANT, this.Content.Load<Texture2D>("hexagon_white"));
            hexagons.Add(Bug.Type.QUEEN, this.Content.Load<Texture2D>("hexagon_red"));
            hexagons.Add(Bug.Type.SPIDER, this.Content.Load<Texture2D>("hexagon_blue"));
            */





        }

        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            // TODO: Add your update logic here

            base.Update(gameTime);
        }

        private void DrawField(string color, Bug.Type bugType, int x, int y, int width, int height)
        {
            _spriteBatch.Draw(hexagonTextures[color], new Rectangle(x, y, width, height), Color.White);
            _spriteBatch.Draw(bugTextures[bugType], new Rectangle(x, y, width, height), Color.White);
        }

        private void DrawSides()
        {
            //left side
            GraphicsDevice.SetRenderTarget(rightSide);
            GraphicsDevice.Clear(Color.White);
            _spriteBatch.Begin();

            int i = 0;
            foreach (Bug.Type t in Enum.GetValues(typeof(Bug.Type)))
            {
                DrawField("red", t, 0, i * 150, 100, 100);
                _spriteBatch.DrawString(font, (i + 1).ToString(), new Vector2(120, i * 150 + 50), Color.Black);
                i++;
            }
            _spriteBatch.End();


            //right side
            GraphicsDevice.SetRenderTarget(leftSide);
            GraphicsDevice.Clear(Color.White);
            _spriteBatch.Begin();

            i = 0;
            foreach (Bug.Type t in Enum.GetValues(typeof(Bug.Type)))
            {
                DrawField("blue", t, 0, i * 150, 100, 100);
                _spriteBatch.DrawString(font, (4-i).ToString(), new Vector2(120, i * 150 + 50), Color.Black);
                i++;
            }
            _spriteBatch.End();

            GraphicsDevice.SetRenderTarget(null);

        }

        protected override void Draw(GameTime gameTime)
        {
            /*
            var viewModelMtx = viewModel.getBoardMatrix();
            GraphicsDevice.Clear(Color.White);

            _spriteBatch.Begin();

            var destRect = new Rectangle(0, 0, 200, 600);
            _spriteBatch.Draw(leftSide, destRect, Color.Black);
            */

            DrawSides();
            GraphicsDevice.Clear(Color.Black);
            _spriteBatch.Begin();
            _spriteBatch.Draw(rightSide, new Rectangle(600, 0, 200, 600), Color.White);
            _spriteBatch.Draw(leftSide, new Rectangle(0, 0, 200, 600), Color.White);
            _spriteBatch.End();
                
            /*
            GraphicsDevice.SetRenderTarget(right);
            _spriteBatch.Draw(hexagon_texture, new Rectangle(10, 10, 40, 40), Color.White);
            GraphicsDevice.SetRenderTarget(null);
            */



            /*
            var destRect = new Rectangle(100, 100, 160, 160);
            _spriteBatch.Draw(hexagon_texture, destRect, Color.White);
            */

            /*

            int q = 0;
            Bug.Type[] lst = new Bug.Type[4];
            foreach (Bug.Type t in Enum.GetValues(typeof(Bug.Type)))
            {
                lst[q] = t;
                q++;
            }

            
            for(int i=0; i<5; i++)
            {
                for(int j=0; j<5; j++)
                {
                    //var texture = hexagons[viewModelMtx[i, j]];
                    var texture = hexagon_texture;
                    Debug.WriteLine(viewModelMtx[i,j]);

                    int x = 6 * i + 3 * (j % 2);
                    int y = 2 * j;
                    var destRect = new Rectangle(x*40,y*40,4*40,4*40);
                    _spriteBatch.Draw(texture, destRect, Color.White);
                    _spriteBatch.Draw(bugTextures[lst[j%4]], destRect, Color.White);
                }
            }

            _spriteBatch.End();
            
            */
            base.Draw(gameTime);
        }
    }
}
