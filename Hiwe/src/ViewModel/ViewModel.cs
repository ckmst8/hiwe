﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using Hiwe.Model;

namespace Hiwe
{
    class ViewModel
    {

        private Model.Model model;
        private Board board;

        public ViewModel()
        {
            model = new Model.Model();

            var queen1 = Bug.Create(Bug.Type.QUEEN);
            model.PlaceBug(new Pos(0, 0), queen1);
            var positions = model.Board.Positions;
            foreach(Pos p in positions)
            {
                Console.WriteLine(p);
            }

        }

        public Bug.Type[,] getBoardMatrix()
        {
            var ret = new Bug.Type[5, 5];
            int k = 0;
            for(int i = 0; i<5; i++)
            {
                for(int j=0; j<5; j++)
                {
                    ret[i, j] = (Bug.Type)(k%3);
                    k++;
                }
            }

            return ret;
        }
    }
}
