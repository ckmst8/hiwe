﻿using Hiwe.Model;
using System;
using System.Diagnostics;

namespace Hiwe
{
    public static class Program
    {
        [STAThread]
        static void Main()
        {
            using (var game = new Game1())
                game.Run();
        }
    }
}
