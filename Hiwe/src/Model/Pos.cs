﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Diagnostics;
using System.Reflection.Metadata.Ecma335;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace Hiwe.Model
{

    public class Pos
    {
        public const int DIR_TL = 0;
        public const int DIR_T = 1;
        public const int DIR_TR = 2;
        public const int DIR_BR = 3;
        public const int DIR_B = 4;
        public const int DIR_BL = 5;

        private int row,col;
        private (int, int) point; // for quick hashing

        public Pos(int row, int col)
        {
            this.row = row;
            this.col = col;
            this.point = (row, col);
        }

        public Pos(Pos otherPos) : this(otherPos.Row, otherPos.Col)
        { }

        public int Row { 
            get { return row; }
            set { row = value; point.Item1 = value; }
        }

        public int Col { 
            get { return col; }
            set { col = value; point.Item2 = value; }
        }

        public Pos P {
            get { return new Pos(P); }
            set { row = value.Row; col = value.Col; }
        }

        public Pos PosT
        {
            get {
                return new Pos(row - 2, col);
            }
        }

        public Pos PosTR
        {
            get {
                if (row % 2 == 0) return new Pos(row - 1, col);
                return new Pos(row - 1, col + 1);
            }
        }

        public Pos PosTL
        {
            get {
                if (row % 2 == 0) return new Pos(row - 1, col - 1);
                return new Pos(row - 1, col);
            }
        }

        public Pos PosB
        {
            get {
                return new Pos(row + 2, col);
            }
        }

        public Pos PosBR
        {
            get {
                if (row % 2 == 0) return new Pos(row + 1, col);
                return new Pos(row + 1, col + 1);
            }
        }

        public Pos PosBL
        {
            get {
                if (row % 2 == 0) return new Pos(row + 1, col - 1);
                return new Pos(row + 1, col);
            }
        }

        /// <summary>
        /// Tells how B relates to A. Example: B is in top of A then Relation(A,B)=DIR_T
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns>Post.DIR_* or -1 if they are not adjecent</returns>
        public int Relation(Pos b)
        {
            if (PosTL == b) return DIR_TL;
            if (PosT == b) return DIR_T;
            if (PosTR == b) return DIR_TR;
            if (PosB == b) return DIR_B;
            if (PosBL == b) return DIR_BL;
            if (PosBR == b) return DIR_BR;
            return -1;
        }

        public override bool Equals(Object obj)
        {
            if ((obj == null) || !this.GetType().Equals(obj.GetType())) {
                return false;
            } else {
                Pos o = (Pos)obj;
                return (row == o.row) && (col == o.col);
            }
        }

        public override int GetHashCode()
        {
            //return (row << 2) ^ col;
            //return (row,col).GetHashCode();
            return point.GetHashCode();
        }

        public override string ToString()
        {
            return String.Format("Pos_{0}_{1}", row, col);
        }

        public static bool operator ==(Pos a, Pos b)
        {
            return a.Equals(b);
        }

        public static bool operator !=(Pos a, Pos b)
        {
            return !(a == b);
        }

    }
}
