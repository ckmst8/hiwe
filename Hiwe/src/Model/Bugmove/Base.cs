﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using Hiwe.Model;

namespace Hiwe.Model.Bugmove
{
    public abstract class Base
    {
        /// <summary>
        /// A list of Fields where the parameter bug may move onto.
        /// </summary>
        /// <param name="b">the bug which we want to move</param>
        /// <returns>A list of possible places to move. The list is empty (but not null!) whenever the bug is unable to move!</returns>
        public abstract List<Pos> getPossibleMoves(Bug b);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="b"></param>
        /// <returns>Returns whether the given bug may move.</returns>
        protected abstract bool canMove(Bug b);
        /*{
            return (IsNotEncircled(b) && OnCircle(b) && IsNotBlocked(b));
        }*/

        /// <summary>
        /// 
        /// </summary>
        /// <param name="b"></param>
        /// <returns>returns if the bug is not encircled by other bugs</returns>
        protected bool IsNotEncircled(Bug b)
        {
            Field bugField = Board.instance().GetField(b.Pos);///he's on that field

            if (Board.instance().GetFieldsAroundL(bugField.Pos).Count < 6)
            {
                return true;
            }

            bool Encircled = true;
            foreach (Field f in Board.instance().GetFieldsAroundL(bugField.Pos))
            {
                Encircled = Encircled && (f.GetBugs().Count > 0); ///there's at least one bug on the adjacent field
            }

            return !Encircled;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="b"></param>
        /// <returns>returns if the bug is on a circle on the mapgraph.</returns>
        protected bool OnCircle(Bug b)
        {
            ///"Removing" the initial node, and determining whether our graph is still connected
            ///This can be determined with BFS starting from the smallest neighbourg component, and if we reach all *neighbourg* component
            ///we are fine

            Field initialField = Board.instance().GetField(b.Pos);
            /*List< List<Pos> > Components = new List<List<Pos>>();
            List<Pos> curComp = new List<Pos>();*/


            ///counting the number of neighbourg components
            //int neighbourgComponentCount = 0;
            /*int longestComponent = 0;
            int longestComponentLength = 0;
            int componentLength = 0;*/

            /*DIR_TL = 0;
            DIR_T = 1;
            DIR_TR = 2;
            DIR_BR = 3;
            DIR_B = 4;
            DIR_BL = 5;*/

            //bool prevNul = true;
            /*foreach(Pos f in Board.instance().GetFieldsAroundA(initialField))
            {
                if(f == null || !f.HasBugs())
                {
                    prevNul = true;
                    if(curComp.Count > 0)
                    {
                        List<Field> addList = new List<Field>(curComp);
                        Components.Add(addList);
                        curComp.Clear();
                    }
                    ///for now, we do not optimize with the longestcomponent
                }
                else
                {
                    if(prevNul)
                    {
                        prevNul = false;
                        curComp.Add(f);
                        neighbourgComponentCount++;
                    }
                    else
                    {
                        curComp.Add(f);
                    }
                }
            }*/

            /*if(Board.instance().GetFieldsAroundA(initialField)[0].HasBugs() && Board.instance().GetFieldsAroundA(initialField)[5].HasBugs())
            {
                neighbourgComponentCount = Math.Min(neighbourgComponentCount - 1, 1); ///we have at least one component, but the first and last component is the same
            }*/



            ///starting the BFS
            ///

            int visitedCount = 1;
            List<Pos> process = new List<Pos>();
            List<Pos> visited = new List<Pos>();

            if (Board.instance().GetFieldsAroundL(initialField.Pos).Count > 0)
            {

                visited.Add(Board.instance().GetFieldsAroundL(initialField.Pos).ElementAt(1).Pos);
                visited.Add(initialField.Pos);

                ///IMPORTANT! WE ONLY ADD ONE NEIGHBOUR
                process.Add(Board.instance().GetFieldsAroundL(initialField.Pos).ElementAt(1).Pos);


                while (visitedCount < Board.instance().TakenFieldsNum && process.Count > 0)
                {
                    Pos curpos = process.ElementAt(0);
                    process.RemoveAt(0);

                    Field[] curField = Board.instance().GetFieldsAroundA(curpos);

                    foreach (Field f in curField)
                    {
                        Pos pos = f.Pos;
                        if (visited.Contains(pos) || process.Contains(pos))
                        {
                            continue;
                        }
                        else
                        {
                            process.Add(pos);
                        }
                    }
                    visited.Add(curpos);
                    visitedCount++;
                }
                return (visitedCount == Board.instance().TakenFieldsNum);

            }


            return false; ///I'm not allowing to move the initial bug!
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="b"></param>
        /// <returns>returns if there is another bug on the top of him.</returns>
        protected bool IsNotBlocked(Bug b)
        {
            Field bugField = Board.instance().GetField(b.Pos); ///by this, we can determine the exact place of our bug on the board

            return (bugField.GetBugs()[bugField.GetBugs().Count - 1] == b); ///checking if b is the last element on the list
            //return true;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="b"></param>
        /// <param name="target"></param>
        /*public void moveBug(Bug b, Field target)
        {
            if(canMove(b))
            {
                if(getPossibleMoves(b).Contains(target))
                {
                    b.Pos = target.Pos;
                    target.
                }
            }
        }*/


    }
}
