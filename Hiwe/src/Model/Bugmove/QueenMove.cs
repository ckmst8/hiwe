﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace Hiwe.Model.Bugmove
{
    public class QueenMove : Base
    {


        public override List<Pos> getPossibleMoves(Bug b)
        {
            List<Pos> PossibleMoves = new List<Pos>();
            if (canMove(b))
            {
                Pos bugPos = b.Pos;


                foreach(Field f in Board.instance().GetFieldsAroundL(bugPos))
                {
                    if(f.HasBug == false)
                    {
                        PossibleMoves.Add(f.Pos);
                    }
                }
            }
            return PossibleMoves;

        }


        protected override bool canMove(Bug b)
        {
            ///A Queen may move to an adjacent non-taken field if is on a circle.

            if (OnCircle(b))
            {
                return IsNotBlocked(b) && IsNotEncircled(b);
            }
            return false;
        }
    }




}
