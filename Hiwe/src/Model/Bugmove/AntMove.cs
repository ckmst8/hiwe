﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hiwe.Model.Bugmove
{
    public class AntMove : Base
    {
        public override List<Pos> getPossibleMoves(Bug b)
        {
            List<Pos> PossibleMoves = new List<Pos>();
            if (canMove(b))
            {
                Pos bugPos = b.Pos;
                PossibleMoves.Add(b.Pos); ///adding some temporary items

                Queue<Pos> proc = new Queue<Pos>(); ///the currently processing fields
                HashSet<Pos> finish = new HashSet<Pos>(); ///the already processed fields

                foreach(Field f in Board.instance().GetFieldsAroundL(b.Pos))
                {
                    if(f.HasBug == false)
                    {
                        proc.Enqueue(f.Pos);
                    }
                }

                while(proc.Count > 0)
                {
                    Pos p = proc.Dequeue();


                    foreach(Field f in Board.instance().GetFieldsAroundL(p))
                    {
                        if(proc.Contains(f.Pos) == false && finish.Contains(f.Pos) == false)
                        {
                            proc.Enqueue(f.Pos);
                        }

                        if(PossibleMoves.Contains(f.Pos))
                        {
                            int rel = f.Pos.Relation(p);

                            ///either the next or prev must be empty to be reachable by the ant
                            int caseA = (rel + 1) % 6;
                            int caseB = (rel - 1) % 6;
                        
                            if(Board.instance().GetFieldAt(p,caseA).HasBug == false || Board.instance().GetFieldAt(p, caseB).HasBug == false)
                            {
                                PossibleMoves.Add(f.Pos);
                                break;
                            }
                            ///possible optimisation: if A has empty neighbour B, that means B is reachable too!
                        }
                    }

                    finish.Add(p);
                }

                PossibleMoves.Remove(b.Pos); ///we must erease the temporary first item
            }
            return PossibleMoves;

        }


        protected override bool canMove(Bug b)
        {
            ///An Ant may move if not encircled and not blocked

            if (OnCircle(b))
            {
                return IsNotBlocked(b) && IsNotEncircled(b);
            }
            return false;
        }
    }
}
