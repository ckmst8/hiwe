﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Reflection.Metadata.Ecma335;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;

namespace Hiwe.Model
{
    public class Board
    {
        private Dictionary<Pos, Field> fldsByPos = new Dictionary<Pos, Field>();

        private static Board singl;
        private int topRow;
        private int leftCol;

        public static Board instance()
        {
            if (singl == null)
            {
                singl = new Board();
            }
            return singl;
        }

        private Board()
        {
            Clear();
        }

        /// <summary>
        /// most top position (minimum Pos.Row value)
        /// </summary>
        public int PosTopRow
        {
            get { return topRow; }
        }

        /// <summary>
        /// most left position (minimum Pos.Col value)
        /// </summary>
        public int PosLeftCol
        {
            get { return leftCol; }
        }

        /// <summary>
        /// 
        /// </summary>
        private Pos OuterPos
        {
            get
            {
                foreach (var pos in fldsByPos.Keys)
                {
                    if(pos.Col == leftCol)
                    {
                        return pos;
                    }
                }
                return null;
            }
        }

        /// <summary>
        /// Returns a list of all the fields (order is undeterministic)
        /// </summary>
        public List<Field> Fields
        {
            get
            {
                List<Field> flds = new List<Field>();
                foreach (var fld in fldsByPos.Values)
                {
                    flds.Add(fld);
                }
                return flds;
            }
        }

        /// <summary>
        /// Returns a list of all the positions that has a field (order is undeterministic)
        /// </summary>
        public List<Pos> Positions
        {
            get
            {
                List<Pos> flds = new List<Pos>();
                foreach (var pos in fldsByPos.Keys)
                {
                    flds.Add(pos);
                }
                return flds;
            }
        }

        /// <summary>
        /// How many fields have at least 1 bug on it
        /// </summary>
        public int TakenFieldsNum
        {
            get
            {
                int cnt = 0;
                foreach (var fld in fldsByPos.Values)
                {
                    if (fld.HasBugs()) ++cnt;
                }
                return cnt;
            }
        }

        /// <summary>
        /// Total number of fields
        /// </summary>
        public int FieldsNum
        {
            get { return fldsByPos.Count; }
        }

        /// <summary>
        /// Creates and returns the new field
        /// </summary>
        /// <param name="pos"></param>
        /// <param name="allowDuplicate">Should we create a new Field even if one exist?</param>
        /// <returns>The field at that position</returns>
        private Field CreateField(Pos pos, bool allowDuplicate = false)
        {
            // place the field
            Field fld;
            if (!allowDuplicate && HasField(pos)) {
                fld = GetField(pos);
            } else {
                fld = new Field(pos);
                fldsByPos.Add(fld.Pos, fld);
            }

            // recalc most top-left positions
            if (pos.Row < topRow) topRow = pos.Row;
            if (pos.Col < leftCol) leftCol = pos.Col;

            return fld;
        }

        /// <summary>
        /// Delete by Pos obj
        /// </summary>
        /// <param name="pos"></param>
        /// <returns>bool: was there one or not</returns>
        private bool DelField(Pos pos)
        {
            bool has = HasField(pos);
            if(has)
            {
                fldsByPos.Remove(pos);
            }
            return has;
        }

        /// <summary>
        /// By Pos obj
        /// </summary>
        /// <param name="pos"></param>
        /// <returns>bool</returns>
        public bool HasField(Pos pos)
        {
            return fldsByPos.ContainsKey(pos);
        }

        /// <summary>
        /// Returns the field on this position (or null)
        /// </summary>
        /// <param name="pos"></param>
        /// <returns>Field obj OR null</returns>
        public Field GetField(Pos pos)
        {
            try {
                return fldsByPos[pos];
            } catch (KeyNotFoundException) {
                return null;
            }
        }

        /// <summary>
        /// </summary>
        /// <param name="pos"></param>
        /// <returns>List of Pos objects. Fields that don't exist are not in the list.</returns>
        public List<Pos> GetPositionsAroundL(Pos pos)
        {
            List<Pos> poss = new List<Pos>();
            if (HasField(pos.PosT)) poss.Add(pos.PosT);
            if (HasField(pos.PosTR)) poss.Add(pos.PosTR);
            if (HasField(pos.PosTL)) poss.Add(pos.PosTL);
            if (HasField(pos.PosB)) poss.Add(pos.PosB);
            if (HasField(pos.PosBR)) poss.Add(pos.PosBR);
            if (HasField(pos.PosBL)) poss.Add(pos.PosBL);
            return poss;
        }

        /// <summary>
        /// </summary>
        /// <param name="pos"></param>
        /// <returns>List of Field objects. Fields that don't exist are not in the list.</returns>
        public List<Field> GetFieldsAroundL(Pos pos)
        {
            List<Field> flds = new List<Field>();
            foreach (Pos p in GetPositionsAroundL(pos))
            {
                flds.Add(GetField(p));
            }
            return flds;
        }

        /// <summary>
        /// </summary>
        /// <param name="pos"></param>
        /// <returns>Pos[6] (use Model.Pos.DIR_* for indexing. If an index has no Field, the value is null.)</returns>
        public Pos[] GetPositionsAroundA(Pos pos)
        {
            Pos[] poss = new Pos[6];
            poss[Pos.DIR_T] = (HasField(pos.PosT) ? pos.PosT : null);
            poss[Pos.DIR_TR] = (HasField(pos.PosTR) ? pos.PosTR : null);
            poss[Pos.DIR_TL] = (HasField(pos.PosTL) ? pos.PosTL : null);
            poss[Pos.DIR_B] = (HasField(pos.PosB) ? pos.PosB : null);
            poss[Pos.DIR_BR] = (HasField(pos.PosBR) ? pos.PosBR : null);
            poss[Pos.DIR_BL] = (HasField(pos.PosBL) ? pos.PosBL : null);
            return poss;
        }

        /// <summary>
        /// </summary>
        /// <param name="pos"></param>
        /// <returns>Field[6] (use Model.Pos.DIR_* for indexing. If an index has no Field, the value is null.)</returns>
        public Field[] GetFieldsAroundA(Pos pos)
        {
            Field[] flds = new Field[6];
            Pos[] poss = GetPositionsAroundA(pos);
            for(int i=0; i<poss.Length; ++i)
            {
                flds[i] = (poss[i] == null) ? null : GetField(poss[i]);
            }
            return flds;
        }

        /// <summary>
        /// Creates the field (if doesnt exist yest), and all the missing surrounding fields too.
        /// </summary>
        /// <param name="pos"></param>
        /// <returns>List of Pos objects: The field for the given pos is at index 0, followed by the newly created surrounding fields</returns>
        public List<Field> PlaceFieldL(Pos pos)
        {
            List<Field> newFlds = new List<Field>();
            newFlds.Add(CreateField(pos));
            if (!HasField(pos.PosT))  newFlds.Add(CreateField(pos.PosT));
            if (!HasField(pos.PosTR)) newFlds.Add(CreateField(pos.PosTR));
            if (!HasField(pos.PosTL)) newFlds.Add(CreateField(pos.PosTL));
            if (!HasField(pos.PosB))  newFlds.Add(CreateField(pos.PosB));
            if (!HasField(pos.PosBR)) newFlds.Add(CreateField(pos.PosBR));
            if (!HasField(pos.PosBL)) newFlds.Add(CreateField(pos.PosBL));
            return newFlds;
        }

        /// <summary>
        /// Creates the field (if doesnt exist yest), and all the missing surrounding fields too
        /// </summary>
        /// <param name="pos"></param>
        /// <returns>Field[7]: The field for the given pos is at index 6, for the surrounding field use Model.Pos.DIR_* for indexing. If an index has no NEW Field, the value is null.</returns>
        public Field[] PlaceFieldA(Pos pos)
        {
            Field[] flds = new Field[7];
            flds[6] = CreateField(pos);
            flds[Pos.DIR_T]  = (!HasField(pos.PosT)  ? CreateField(pos.PosT)  : null);
            flds[Pos.DIR_TR] = (!HasField(pos.PosTR) ? CreateField(pos.PosTR) : null);
            flds[Pos.DIR_TL] = (!HasField(pos.PosTL) ? CreateField(pos.PosTL) : null);
            flds[Pos.DIR_B]  = (!HasField(pos.PosB)  ? CreateField(pos.PosB)  : null);
            flds[Pos.DIR_BR] = (!HasField(pos.PosBR) ? CreateField(pos.PosBR) : null);
            flds[Pos.DIR_BL] = (!HasField(pos.PosBL) ? CreateField(pos.PosBL) : null);
            return flds;
        }

        /// <summary>
        /// Returns the 1 field that is into the given direction
        /// </summary>
        /// <param name="pos"></param>
        /// <param name="dir"></param>
        /// <returns>If there is no field it returns null</returns>
        public Field GetFieldAt(Pos pos, int dir)
        {
            return GetFieldsAroundA(pos)[dir];
        }

        /// <summary>
        /// Is there a field into the given direction?
        /// </summary>
        /// <param name="pos"></param>
        /// <param name="dir"></param>
        /// <returns></returns>
        public bool HasFieldAt(Pos pos, int dir)
        {
            return GetFieldAt(pos, dir) != null;
        }

        /// <summary>
        /// Removes everything
        /// </summary>
        public void Clear()
        {
            fldsByPos.Clear();
            topRow = 0;
            leftCol = 0;
        }

        /// <summary>
        /// Places the bug on the given field. If there was already a bug, it will put it on top of it
        /// </summary>
        /// <param name="pos"></param>
        /// <param name="bug"></param>
        /// <returns>The number of bugs on the field after the placement</returns>
        /// <exception cref="ArgumentException">field doesnt exist</exception>
        public int PlaceBug(Pos pos, Bug bug)
        {
            if (!HasField(pos))
            {
                throw new ArgumentException(String.Format("There's no field at position {0}", pos.ToString()), "pos");
            }
            return GetField(pos).PushBug(bug);
        }

        /// <summary>
        /// Returns the most recently placed bug on this position (or null).
        /// If you wanna also remove the bug, use PopBug()
        /// </summary>
        /// <param name="pos"></param>
        /// <returns>Bug obj OR null</returns>
        public Bug GetBugTop(Pos pos)
        {
            return (HasField(pos)) ? GetField(pos).BugTop : null;
        }

        /// <summary>
        /// Removes and returns the most recently placed bug on this position (or null).
        /// If dont wanna remove the bug, use GetBugTop()
        /// </summary>
        /// <param name="pos"></param>
        /// <returns></returns>
        public Bug PopBug(Pos pos)
        {
            return (HasField(pos)) ? GetField(pos).PopBug() : null;
        }

        /// <summary>
        /// Removes and returns the most recently added bug
        /// </summary>
        /// <param name="pos"></param>
        /// <returns>Bug obj OR null</returns>
        /// <exception cref="ArgumentException">field doesnt exist</exception>
        public Bug RemoveBug(Pos pos)
        {
            if (!HasField(pos))
            {
                throw new ArgumentException(String.Format("There's no field at position {0}", pos.ToString()), "pos");
            }
            return GetField(pos).PopBug();
        }

        /// <summary>
        /// Remove + Place with error checks
        /// </summary>
        /// <param name="posFrom"></param>
        /// <param name="posTo"></param>
        /// <returns>The number of bugs on the field after the placement</returns>
        /// <exception cref="ArgumentException">any of the fields doest exist, or no bug on the posFrom</exception>
        public int MoveBug(Pos posFrom, Pos posTo)
        {
            if (!HasField(posFrom))
            {
                throw new ArgumentException(String.Format("There's no field at position {0}", posFrom.ToString()), "posFrom");
            }
            if (!HasField(posTo))
            {
                throw new ArgumentException(String.Format("There's no field at position {0}", posTo.ToString()), "posTo");
            }
            if(!GetField(posFrom).HasBugs())
            {
                throw new ArgumentException(String.Format("There are no bugs at the position {0}", posFrom.ToString()), "posFrom");
            }
            return PlaceBug(posTo, RemoveBug(posFrom));
        }

        /// <summary>
        /// Bugs on the given position
        /// </summary>
        /// <param name="pos"></param>
        /// <returns>List of Bug objects (can be empty list)</returns>
        public List<Bug> GetBugs(Pos pos)
        {
            return (HasField(pos)) ? GetField(pos).GetBugs() : (new List<Bug>());
        }

    }
}
