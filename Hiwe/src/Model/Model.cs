﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hiwe.Model
{
    class Model
    {

        public Model()
        {
        }

        public Board Board
        {
            get { return Board.instance(); }
        }

        /// <summary>
        /// Bugs the can be placed on the board
        /// </summary>
        /// <returns>List of Bugs (or empty list)</returns>
        public List<Bug> GetAvaliableBugs()
        {
            var bugs = new List<Bug>();
            bugs.Add(Bug.Create(Bug.Type.QUEEN));
            bugs.Add(Bug.Create(Bug.Type.ANT));
            bugs.Add(Bug.Create(Bug.Type.ANT));
            bugs.Add(Bug.Create(Bug.Type.SPIDER));
            bugs.Add(Bug.Create(Bug.Type.SPIDER));
            bugs.Add(Bug.Create(Bug.Type.SPIDER));
            return bugs;
        }

        /// <summary>
        /// Positions we can place the given bug on the board
        /// </summary>
        /// <param name="bug"></param>
        /// <returns>List of Pos (or empty list)</returns>
        public List<Pos> GetPossiblePlaceList(Bug bug)
        {
            // MOCK
            return Board.Positions;
        }

        /// <summary>
        /// Positions the given Bug can move to
        /// </summary>
        /// <param name="id"></param>
        /// <returns>List of Pos (or empty list)</returns>
        public List<Pos> GetPossibleMoveList(Bug bug)
        {
            // MOCK
            // return bug.GetPossibleMoves();
            var flds = Board.GetFieldsAroundL(bug.Pos);
            var poss = new List<Pos>();
            foreach (Field fld in flds) poss.Add(fld.Pos);
            return poss;
        }

        /// <summary>
        /// Places the bug on the given field. If there was already a bug, it will put it on top of it.
        /// If the field doesn exist, it will create it.
        /// </summary>
        /// <param name="pos"></param>
        /// <param name="bug"></param>
        /// <returns>The number of bugs on the field after the placement</returns>
        public int PlaceBug(Pos pos, Bug bug)
        {
            if(!Board.HasField(pos))
            {
                Board.PlaceFieldA(pos);
            }
            return Board.PlaceBug(pos, bug);
        }

        /// <summary>
        /// Remove + Place with error checks
        /// </summary>
        /// <param name="posFrom"></param>
        /// <param name="posTo"></param>
        /// <returns>The number of bugs on the field after the placement</returns>
        /// <exception cref="ArgumentException">any of the fields doest exist, or no bug on the posFrom</exception>
        public int MoveBug(Pos posFrom, Pos posTo)
        {
            return Board.MoveBug(posFrom, posTo);
        }

        /// <summary>
        /// Removes everything from the board
        /// </summary>
        public void ClearBoard()
        {
            Board.Clear();
        }
    }
}
