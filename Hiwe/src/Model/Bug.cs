﻿using System;
using System.Collections.Generic;
using System.Text;
using Hiwe.Model.Bugmove;

namespace Hiwe.Model
{

    public class Bug
    {
        public enum Type { QUEEN, ANT, SPIDER, GRASSHOPPER }

        private Type typ;
        private Bugmove.Base move;
        private Pos pos;

        /// <summary>
        /// Creates a bug
        /// </summary>
        /// <param name="typ">Use Model.Bug.Type.* constants</param>
        /// <returns></returns>
        /// <exception cref="ArgumentException"></exception>
        public static Bug Create(Type typ)
        {
            switch (typ)
            {
                case Type.QUEEN: return new Bug(typ, new Bugmove.QueenMove());
                case Type.ANT: return new Bug(typ, new Bugmove.AntMove());
                case Type.SPIDER: return new Bug(typ, new Bugmove.Simple1());
                default: throw new ArgumentException(string.Format("Invalid Bug.Type: {0}", typ), "typ");
            }
        }

        /// <summary>
        /// Use Model.Bug.Typ.* constants
        /// </summary>
        /// <param name="typ">Use Model.Bug.Type.* constants</param>
        /// <param name="move"></param>
        private Bug(Type typ, Bugmove.Base move)
        {
            this.typ = typ;
            this.move = move;
        }

        /// <summary>
        /// Model.Bug.Typ.* constant
        /// </summary>
        public Type Typ
        {
            get { return typ; }
        }

        public Pos Pos
        {
            get { return pos; }
            set { pos = value; }
        }
    }
}
