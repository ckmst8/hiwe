﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hiwe.Model
{
    public class Player
    {
        private int antcap;

        private int grasshoppercap;

        private int spidercap;

        public Bug Queen { get; private set; }

        public Player OtherPlayer { get; set; }
        public int Id { get; private set; }
        public int StepDone { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="_id"></param>
        /// <param name="_otherplayer">the player which takes the next turn</param>
        /// <param name="_antcap">max number of ants</param>
        /// <param name="_grasshoppercap">max number of grasshopper</param>
        /// <param name="_spidercap">max number of spider</param>
        public Player(int _id, Player _otherplayer = null, int _antcap = 0, int _grasshoppercap = 0, int _spidercap = 0)
        {
            OtherPlayer = _otherplayer;
            Id = _id;
            StepDone = 0;
            antcap = _antcap;
            grasshoppercap = _grasshoppercap;
            spidercap = _spidercap;

            Queen = Bug.Create(Bug.Type.QUEEN, this);

            for (int i = 0; i < antcap; i++)
            {
                Bug.Create(Bug.Type.ANT, this);
            }

            for (int i = 0; i < grasshoppercap; i++)
            {
                Bug.Create(Bug.Type.GRASSHOPPER, this);
            }

            for (int i = 0; i < spidercap; i++)
            {
                Bug.Create(Bug.Type.SPIDER, this);
            }
        }

        /// <summary>
        /// Gives a bug from the hand. Returns null if there's no available bug!
        /// </summary>
        /// <param name="p"></param>
        /// <param name="typ"></param>
        /// <returns>Returns null if there's no available bug!</returns>
        public Bug PlaceBugFromHand(Pos p, Bug.Type typ)
        {
            if (GetHandBugListByType(typ).Count == 0) return null;
            Bug b = GetHandBugListByType(typ)[0];
            b.isOnBoard = true;
            b.Pos = p;
            return b;
        }

        ///reset player
        public void Reset()
        {
            foreach (Bug b in Bug.GetAllBugs())
            {
                if (b.Player == this)
                {
                    b.isOnBoard = false;
                    b.Pos = null;
                }
            }
        }

        /// <summary>
        /// List of available bugs for the player filtered by a type
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public List<Bug> GetHandBugListByType(Bug.Type type)
        {
            List<Bug> ListByType = new List<Bug>();
            foreach (Bug b in Bug.GetAllBugs())
            {
                if (b.Player == this && b.Typ == type && b.isOnBoard == false)
                {
                    ListByType.Add(b);
                }
            }

            return ListByType;
        }

        /// <summary>
        /// List of available bugs for the player
        /// </summary>
        /// <returns></returns>
        public List<Bug> GetHandBugList()
        {
            List<Bug> listofbugs = new List<Bug>();
            foreach (Bug b in Bug.GetAllBugs())
            {
                if (b.Player == this && b.isOnBoard == false)
                {
                    listofbugs.Add(b);
                }
            }

            return listofbugs;
        }

        public override string ToString()
        {
            return string.Format("Player{0}", Id);
        }

        public override bool Equals(Object obj)
        {
            if ((obj == null) || !this.GetType().Equals(obj.GetType()))
            {
                return false;
            }
            else
            {
                Player o = (Player)obj;
                return (Id == o.Id);
            }
        }

        public override int GetHashCode() { return Id.GetHashCode(); }

        public static bool operator ==(Player a, Player b)
        {
            if (a is null) return false;
            return a.Equals(b);
        }

        public static bool operator !=(Player a, Player b)
        {
            return !(a == b);
        }
    }
}
