﻿using System;
using System.Collections.Generic;
using System.Numerics;
using System.Reflection.Metadata.Ecma335;
using System.Text;

namespace Hiwe.Model
{
    public class Field
    {
        private Pos pos;
        private List<Bug> bugs;

        public Field(Pos pos)
        {
            this.pos = pos;
            this.bugs = new List<Bug>();
        }

        public Pos Pos
        {
            get { return pos; }
            set { pos.P = value; }
        }

        /// <summary>
        /// How many bugs does it have?
        /// </summary>
        /// <returns></returns>
        public int BugsCount
        {
            get { return CountBugs(); }
        }

        /// <summary>
        /// How many bugs does it have?
        /// </summary>
        /// <returns></returns>
        public int CountBugs()
        {
            return bugs.Count;
        }

        /// <summary>
        /// Does it have any bugs?
        /// </summary>
        public bool HasBug
        {
            get { return HasBugs(); }
        }

        /// <summary>
        /// Does it have any bugs?
        /// </summary
        public bool HasBugs()
        {
            return (CountBugs() > 0);
        }

        public bool HasBugAt(int i)
        {
            return (CountBugs() > i);
        }

        /// <summary>
        /// Starting at 0
        /// </summary>
        /// <param name="i"></param>
        /// <returns>Bug obj OR null</returns>
        public Bug GetBugAt(int i)
        {
            return HasBugAt(i) ? bugs[i] : null;
        }

        /// <summary>
        /// </summary>
        /// <param name="i"></param>
        /// <returns>Bug obj OR null</returns>
        public Bug BugTop
        {
            get { return GetBugTop();  }
        }

        /// <summary>
        /// Returns the most recently added bug (added with PushBug()).
        /// If you also wanna remove the bug, use PopBug()
        /// </summary>
        /// <param name="i"></param>
        /// <returns>Bug obj OR null</returns>
        public Bug GetBugTop()
        {
            int cnt = CountBugs();
            return (cnt > 0) ? GetBugAt(cnt - 1) : null;
        }

        /// <summary>
        /// </summary>
        /// <returns>List of Bug objects (can be empty list)</returns>
        public List<Bug> Bugs
        {
            get { return GetBugs(); }
        }

        /// <summary>
        /// </summary>
        /// <returns>List of Bug objects (can be empty list)</returns>
        public List<Bug> GetBugs()
        {
            // TODO: this gives away my private attribute!
            return bugs;
        }

        /// <summary>
        /// Add the given bug to the top of the list (also sets Bug.Pos)
        /// </summary>
        /// <param name="bug"></param>
        /// <returns>the number of bug that are there after the push</returns>
        public int PushBug(Bug bug)
        {
            bugs.Add(bug);
            bug.Pos = pos;
            return bugs.Count;
        }

        /// <summary>
        /// Removes and returns the most recently added bug (added with PushBug()).
        /// If you dont wanna remove the bug, use BugTop
        /// </summary>
        /// <returns>Bug obj OR null</returns>
        public Bug PopBug()
        {
            int cnt = bugs.Count;
            if (cnt == 0) return null;
            Bug b = bugs[cnt - 1];
            bugs.RemoveAt(cnt - 1);
            return b;
        }

        public override bool Equals(Object obj)
        {
            if ((obj == null) || !this.GetType().Equals(obj.GetType())) {
                return false;
            } else {
                Field o = (Field)obj;
                return (pos == o.pos);
            }
        }

        public override int GetHashCode()
        {
            //return (row << 2) ^ col;
            return pos.GetHashCode();
        }

        public override string ToString()
        {
            return String.Format("Fld_{0}", pos.ToString());
        }

        public static bool operator ==(Field a, Field b)
        {
            return a.Equals(b);
        }

        public static bool operator !=(Field a, Field b)
        {
            return !(a == b);
        }

    }
}
