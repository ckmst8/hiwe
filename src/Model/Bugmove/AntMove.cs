﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;

namespace Hiwe.Model.Bugmove
{
    public class AntMove : Base
    {
        public override List<Pos> getPossibleMoves(Bug b)
        {
            List<Pos> PossibleMoves = new List<Pos>();
            if (canMove(b))
            {
                Pos bugPos = b.Pos;
                PossibleMoves.Add(b.Pos);

                Queue<Pos> proc = new Queue<Pos>(); ///the currently processing fields
                HashSet<Pos> finish = new HashSet<Pos>(); ///the already processed fields

                foreach(Field f in Board.instance().GetFieldsAroundL(b.Pos))
                {
                    if(f.HasBug == false)
                    {
                        proc.Enqueue(f.Pos);
                    }
                }

                proc.Enqueue(b.Pos);
                while(proc.Count > 0)
                {
                    Pos p = proc.Dequeue();


                    foreach(Field f in Board.instance().GetFieldsAroundL(p))
                    {
                        if (f.HasBug) continue;

                        int rel = f.Pos.Relation(p);

                        ///either the next or prev must be empty to be reachable by the ant
                        int caseA = (rel + 1) % 6;

                        int caseB = rel - 1;
                        if (caseB < 0) caseB += 6;
                        //Debug.WriteLine(caseB);

                        var fldA = Board.instance().GetFieldAt(p, caseA);
                        var fldB = Board.instance().GetFieldAt(p, caseB);


                        if ((fldB is null) || (fldA is null) || (fldA.HasBug == false || fldB.HasBug == false))
                        {
                            Debug.WriteLine("--------------------------");
                            Debug.WriteLine(NumberOfOtherBugs(b, f));
                            Debug.WriteLine("--------------------------");
                            if (NumberOfOtherBugs(b, f) > 0)
                            {
                                PossibleMoves.Add(f.Pos);
                                Debug.WriteLine("koooool");
                            }
                            if (! (proc.Contains(f.Pos) || finish.Contains(f.Pos)) )
                            {
                                proc.Enqueue(f.Pos);
                            }  
                        }

                    }

                    finish.Add(p);
                }

                PossibleMoves.Remove(b.Pos); ///we must erease the temporary first item
            }
            return PossibleMoves;

        }


        protected override bool canMove(Bug b)
        {
            ///An Ant may move if not encircled and not blocked

            if (OnCircle(b))
            {
                return IsNotBlocked(b) && IsNotEncircled(b);
            }
            return false;
        }
    }
}
