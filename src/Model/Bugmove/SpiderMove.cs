﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hiwe.Model.Bugmove
{
    class SpiderMove : Base
    {
        public override List<Pos> getPossibleMoves(Bug b)
        {
            List<Pos> PossibleMoves = new List<Pos>();
            if (canMove(b))
            {
                Pos bugPos = b.Pos;

                Queue<(Pos pos, int round)> proc = new Queue<(Pos pos, int round)>(); ///the currently processing fields
                HashSet<Pos> finish = new HashSet<Pos>(); ///the already processed fields

                /*foreach (Field f in Board.instance().GetFieldsAroundL(b.Pos))
                {
                    if (f.HasBug == false)
                    {
                        proc.Enqueue((f.Pos, 1));
                    }
                }

                proc.Enqueue((b.Pos ,0));
                while (proc.Count > 0)
                {
                    (Pos pos, int round) temp = proc.Dequeue();
                    Pos p = temp.pos;
                    int round = temp.round;


                    foreach (Field f in Board.instance().GetFieldsAroundL(p))
                    {
                        bool contains = false;
                        int roundparam = -1;
                        for (int i = 1; i < 4; i++)
                        {
                            if (proc.Contains((f.Pos, i)))
                            {
                                contains = true;
                                roundparam = i;
                                break;
                            }
                        }
                        if (contains == false && finish.Contains(f.Pos) == false && roundparam < 3)
                        {
                            proc.Enqueue((f.Pos, roundparam+1));
                        }

                        if (PossibleMoves.Contains(f.Pos))
                        {
                            int rel = f.Pos.Relation(p);

                            ///either the next or prev must be empty to be reachable by the ant
                            int caseA = (rel + 1) % 6;
                            if (caseA < 0) caseA += 6;

                            int caseB = (rel - 1) % 6;
                            if (caseB < 0) caseB += 6;
                            //Debug.WriteLine(caseB);

                            var fldA = Board.instance().GetFieldAt(p, caseA);
                            if (fldA is null) continue;

                            var fldB = Board.instance().GetFieldAt(p, caseB);
                            if (fldB is null) continue;

                            if (fldA.HasBug == false || fldB.HasBug == false)
                            {
                                PossibleMoves.Add(f.Pos);
                                break;
                            }
                            ///possible optimisation: if A has empty neighbour B, that means B is reachable too!
                        }
                    }

                    finish.Add(p);
                }

                PossibleMoves.Remove(b.Pos); ///we must erease the temporary first item
                */



                proc.Enqueue((b.Pos, 0));
                while (proc.Count > 0)
                {
                    (Pos pos, int round) tmp = proc.Dequeue();
                    Pos p = tmp.pos;
                    int round = tmp.round + 1;
                    if (round > 3) continue;

                    foreach (Field f in Board.instance().GetFieldsAroundL(p))
                    {
                        if (f.HasBug) continue;
                        int rel = f.Pos.Relation(p);

                        ///either the next or prev must be empty to be reachable by the ant
                        int caseA = (rel + 1) % 6;
                        if (caseA < 0) caseA += 6;

                        int caseB = (rel - 1) % 6;
                        if (caseB < 0) caseB += 6;
                        //Debug.WriteLine(caseB);

                        var fldA = Board.instance().GetFieldAt(p, caseA);
                        var fldB = Board.instance().GetFieldAt(p, caseB);
                        

                        if ( (fldB is null) || (fldA is null) || (fldA.HasBug == false || fldB.HasBug == false))
                        {
                            if (NumberOfOtherBugs(b, f) > 0)
                            {
                                PossibleMoves.Add(f.Pos);
                            }
                            if (!(proc.Contains((f.Pos, 0)) || proc.Contains((f.Pos, 1)) || proc.Contains((f.Pos, 2)) || proc.Contains((f.Pos, 3)) || finish.Contains(f.Pos)))
                            {
                                proc.Enqueue((f.Pos, round));
                            }
                        }
                    }
                }

            }
            return PossibleMoves;

        }


        protected override bool canMove(Bug b)
        {
            ///A Grasshopper may move if not encircled(?) and not blocked

            if (OnCircle(b))
            {
                return IsNotBlocked(b) && IsNotEncircled(b);
            }
            return false;
        }
    }
}
