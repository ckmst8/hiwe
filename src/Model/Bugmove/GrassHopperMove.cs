﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hiwe.Model.Bugmove
{
    public class GrassHopperMove : Base
    {

        public override List<Pos> getPossibleMoves(Bug b)
        {
            List<Pos> PossibleMoves = new List<Pos>();
            if (canMove(b))
            {
                ///we need to start a DFS for each direction until we find an empty field
                for(int i=0; i<6; i++)
                {
                    if(Board.instance().GetFieldAt(b.Pos,i).HasBug)
                    {
                        Field f = Board.instance().GetFieldAt(b.Pos, i);
                        while(f.HasBug)
                        {
                            f = Board.instance().GetFieldAt(f.Pos, i);
                        }
                        if(NumberOfOtherBugs(b,f) > 0)
                        {
                            PossibleMoves.Add(f.Pos);
                        }
                    }
                }

            }
            return PossibleMoves;

        }


        protected override bool canMove(Bug b)
        {
            ///A Grasshopper may move if not encircled(?) and not blocked

            if (OnCircle(b))
            {
                return IsNotBlocked(b) && IsNotEncircled(b);
            }
            return false;
        }
    }
}

