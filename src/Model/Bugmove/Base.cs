﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using Hiwe.Model;

namespace Hiwe.Model.Bugmove
{
    public abstract class Base
    {
        /// <summary>
        /// A list of Fields where the parameter bug may move onto.
        /// </summary>
        /// <param name="b">the bug which we want to move</param>
        /// <returns>A list of possible places to move. The list is empty (but not null!) whenever the bug is unable to move!</returns>
        public abstract List<Pos> getPossibleMoves(Bug b);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="b"></param>
        /// <returns>Returns whether the given bug may move.</returns>
        protected abstract bool canMove(Bug b);
        /*{
            return (IsNotEncircled(b) && OnCircle(b) && IsNotBlocked(b));
        }*/

        /// <summary>
        /// 
        /// </summary>
        /// <param name="b"></param>
        /// <returns>returns if the bug is not encircled by other bugs</returns>
        protected bool IsNotEncircled(Bug b)
        {
            Field bugField = Board.instance().GetField(b.Pos);///he's on that field

            if (Board.instance().GetFieldsAroundL(bugField.Pos).Count < 6)
            {
                return true;
            }

            bool Encircled = true;
            foreach (Field f in Board.instance().GetFieldsAroundL(bugField.Pos))
            {
                Encircled = Encircled && (f.GetBugs().Count > 0); ///there's at least one bug on the adjacent field
            }

            return !Encircled;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="b"></param>
        /// <returns>returns if the bug is on a circle on the mapgraph.</returns>
        protected bool OnCircle(Bug b)
        {
            ///"Removing" the initial node, and determining whether our graph is still connected
            ///This can be determined with BFS starting from the smallest neighbourg component, and if we reach all *neighbourg* component
            ///we are fine

            Field initialField = Board.instance().GetField(b.Pos);
            
            ///starting the BFS
            ///

            int visitedCount = 1;
            List<Pos> process = new List<Pos>();
            List<Pos> visited = new List<Pos>();

            if (Board.instance().GetFieldsAroundL(initialField.Pos).Count > 0) ///needless
            {

                //visited.Add(Board.instance().GetFieldsAroundL(initialField.Pos).ElementAt(1).Pos);
                visited.Add(initialField.Pos);

                ///IMPORTANT! WE ONLY ADD ONE NEIGHBOUR
                int iter = 1;
                Pos temp = Board.instance().GetFieldsAroundL(initialField.Pos).ElementAt(0).Pos;
                while (Board.instance().GetBugs(temp).Count == 0)
                {
                    temp = Board.instance().GetFieldsAroundL(initialField.Pos).ElementAt(iter).Pos;
                    iter++;
                    if(iter > 5) { return false; }
                }
                process.Add(temp);


                while (visitedCount < Board.instance().TakenFieldsNum && process.Count > 0)
                {
                    Pos curpos = process.ElementAt(0);
                    process.RemoveAt(0);

                    Field[] curField = Board.instance().GetFieldsAroundA(curpos);

                    foreach (Field f in curField)
                    {
                        if (f is null) continue;
                        if (f.HasBug == false) continue;

                        Pos pos = f.Pos;
                        if (visited.Contains(pos) || process.Contains(pos))
                        {
                            continue;
                        }
                        else
                        {
                            process.Add(pos);
                        }
                    }
                    visited.Add(curpos);
                    visitedCount++;
                }
                return (visitedCount == Board.instance().TakenFieldsNum);

            }


            return false; ///I'm not allowing to move the initial bug!
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="b"></param>
        /// <returns>returns if there is another bug on the top of him.</returns>
        protected bool IsNotBlocked(Bug b)
        {
            Field bugField = Board.instance().GetField(b.Pos); ///by this, we can determine the exact place of our bug on the board

            return (bugField.GetBugs()[bugField.GetBugs().Count - 1] == b); ///checking if b is the last element on the list
            //return true;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="b"></param>
        /// <param name="target"></param>
        /*public void moveBug(Bug b, Field target)
        {
            if(canMove(b))
            {
                if(getPossibleMoves(b).Contains(target))
                {
                    b.Pos = target.Pos;
                    target.
                }
            }
        }*/

        public int NumberOfOtherBugs(Bug b, Field f)
        {
            int sum = 0;
            foreach(Field field in Board.instance().GetFieldsAroundL(f.Pos))
            {
                if (field is null) continue;
                if(field.Bugs.Contains(b))
                {
                    sum = sum + field.Bugs.Count - 1;
                }
                else
                {
                    sum = sum + field.Bugs.Count;
                }
            }
            return sum;
        }


        public List<Pos> GetPossiblePlaceList(Bug b)
        {
            List<Pos> placelist = new List<Pos>();
            Pos startpos = Board.instance().OuterPos;

            placelist.Add(startpos);



            Queue<Pos> proc = new Queue<Pos>(); ///the currently processing fields
            HashSet<Pos> finish = new HashSet<Pos>(); ///the already processed fields

            foreach (Field f in Board.instance().GetFieldsAroundL(startpos))
            {
                if (f.HasBug == false)
                {
                    proc.Enqueue(f.Pos);
                }
            }

            proc.Enqueue(startpos);
            while (proc.Count > 0)
            {
                Pos p = proc.Dequeue();


                foreach (Field f in Board.instance().GetFieldsAroundL(p))
                {
                    if (f.HasBug) continue;

                    int rel = f.Pos.Relation(p);

                    ///either the next or prev must be empty to be reachable by the ant
                    int caseA = (rel + 1) % 6;

                    int caseB = rel - 1;
                    if (caseB < 0) caseB += 6;
                    //Debug.WriteLine(caseB);

                    var fldA = Board.instance().GetFieldAt(p, caseA);
                    var fldB = Board.instance().GetFieldAt(p, caseB);


                    if ((fldB is null) || (fldA is null) || (fldA.HasBug == false || fldB.HasBug == false))
                    {
                        placelist.Add(f.Pos);
                        if (!(proc.Contains(f.Pos) || finish.Contains(f.Pos)))
                        {
                            proc.Enqueue(f.Pos);
                        }
                    }

                }

                finish.Add(p);
            }


            return placelist;
        }
    }
}
