﻿using System;
using System.Collections.Generic;
using System.Text;

/// <summary>
/// KARINA
/// </summary>
namespace Hiwe.Model
{
    class Model
    {
        public Board Board
        {
            get
            {
                return Board.instance();
            }
        }
        public Player CurrPlayer { get; private set; }
        public Player Player1 { get; private set; }
        public Player Player2 { get; private set; }

        private static Model singl;
        public static Model instance()
        {
            if (singl == null)
            {
                singl = new Model();
            }
            return singl;
        }

        private Model()
        {
            Player1 = new Player(1, null, 3, 3, 3);
            Player2 = new Player(2, Player1, 3, 3, 3);
            Player1.OtherPlayer = Player2;

            NewGame();
        }

        public void NewGame()
        {
            Board.Clear();
            //Board.PlaceFieldA(new Pos(0, 0));
            CurrPlayer = Player1;
        }

        private void SwitchPlayer()
        {
            CurrPlayer = CurrPlayer.OtherPlayer;
        }

        /// <summary>
        /// Winner of the game OR game is still active
        /// </summary>
        /// <returns>
        /// 0 if the game is still active,
        /// 1 if Player1 won,
        /// 2 if Player2 won
        /// </returns>
        public int GetWinner()
        {
            if (Player1.Queen.isOnBoard && Player1.Queen.GetNumberofBugsAround() == 6) { return 2; }
            if (Player2.Queen.isOnBoard && Player2.Queen.GetNumberofBugsAround() == 6) { return 1; }
            return 0;
        }

        /// <summary>
        /// Positions the given Bug can move to
        /// </summary>
        /// <param name="id"></param>
        /// <returns>List of Pos (or empty list)</returns>
        public List<Pos> GetPossiblePlaceList(Bug bug)
        {
            if (bug.isOnBoard) {
                return bug.getPossibleMoves();
            } else {
                return bug.GetPossiblePlaceList();
            }
        }

        /// <summary>
        /// Places the bug on the given field. If there was already a bug, it will put it on top of it.
        /// If moved to empty field, creates empty fields around the new position
        /// If previous field is now empty, deletes the unnecessary empty fields around the old field
        /// </summary>
        /// <param name="pos"></param>
        /// <param name="bug"></param>
        /// <returns>The number of bugs on the field after the placement</returns>
        public bool PlaceBug(Pos pos, Bug bug)
        {
            if (bug.Player != CurrPlayer) return false;

            if (bug.Typ != Bug.Type.QUEEN && !CurrPlayer.Queen.isOnBoard)
            {
                if (bug.isOnBoard)
                {
                    return false;
                }
                else
                {
                    if (CurrPlayer.StepDone > 1) return false;
                }
            }

            if(bug.isOnBoard)
            {
                Board.MoveBug(bug.Pos, pos);
            }
            else
            {
                bug.Pos = pos;
                bug.isOnBoard = true;
                Board.PlaceBug(pos, bug);
            }

            CurrPlayer.StepDone += 1;
            SwitchPlayer();

            return true;
        }

        /// <summary>
        /// Removes everything from the board
        /// </summary>
        public void ClearBoard()
        {
            Board.Clear();
        }
    }
}
