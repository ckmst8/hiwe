﻿using System;
using System.Collections.Generic;
using System.Text;
using Hiwe.Model.Bugmove;

namespace Hiwe.Model
{

    public class Bug
    {
        public enum Type { QUEEN, ANT, SPIDER, GRASSHOPPER }

        public Type Typ { get; set; }
        private Bugmove.Base move;
        public Pos Pos { get; set; }
        public Player Player { get; private set; }
        public bool isOnBoard { get; set; }
        public int Id { get; private set; }

        private static List<Bug> AllBugs = new List<Bug>();
        private static int _next_id = 0;

        /// <summary>
        /// Creates a bug
        /// </summary>
        /// <param name="typ">Use Model.Bug.Type.* constants</param>
        /// <returns></returns>
        /// <exception cref="ArgumentException"></exception>
        public static Bug Create(Type typ, Player player)
        {
            Bug newbug;
            switch (typ)
            {
                case Type.QUEEN: 
                    newbug = new Bug(Bug._next_id, typ, new Bugmove.QueenMove(), player);
                    break;
                case Type.ANT:
                     newbug = new Bug(Bug._next_id, typ, new Bugmove.AntMove(), player);
                    break;
                case Type.SPIDER:
                     newbug = new Bug(Bug._next_id, typ, new Bugmove.SpiderMove(), player);
                    break;
                case Type.GRASSHOPPER:
                     newbug = new Bug(Bug._next_id, typ, new Bugmove.GrassHopperMove(), player);
                    break;

               default: throw new ArgumentException(string.Format("Invalid Bug.Type: {0}", typ), "typ");
            }
            newbug.isOnBoard = false;
            AllBugs.Add(newbug);
            ++Bug._next_id;
            return newbug;
        }

        /// <summary>
        /// Use Model.Bug.Typ.* constants
        /// </summary>
        /// <param name="typ">Use Model.Bug.Type.* constants</param>
        /// <param name="move"></param>
        private Bug(int id, Type typ, Bugmove.Base move, Player player)
        {
            Id = id;
            this.Typ = typ;
            this.move = move;
            this.Player = player;
        }

        public List<Pos> getPossibleMoves()
        {
            return move.getPossibleMoves(this);
        }

        public List<Pos> GetPossiblePlaceList()
        {
            return move.GetPossiblePlaceList(this);
        }

        public static List<Bug> GetAllBugs()
        {
            return AllBugs;
        }

        public override string ToString()
        {
            return "Bug:"+Id+", Pl:"+Player.ToString() + ", Typ:" + Typ.ToString();
        }

        public override bool Equals(Object obj)
        {
            if ((obj == null) || !this.GetType().Equals(obj.GetType()))
            {
                return false;
            }
            else
            {
                Bug o = (Bug)obj;
                return (Id == o.Id);
            }
        }

        public int GetNumberofBugsAround()
        {
            return move.NumberOfOtherBugs(this, Board.instance().GetField(this.Pos));
        }

        public override int GetHashCode()
        {
            //return (row << 2) ^ col;
            //return (row,col).GetHashCode();
            return Id.GetHashCode();
        }

        public static bool operator ==(Bug a, Bug b)
        {
            if (a is null) return false;
            return a.Equals(b);
        }

        public static bool operator !=(Bug a, Bug b)
        {
            return !(a == b);
        }
    }
}
