﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using Hiwe.Model;
using System.Windows.Input;

namespace Hiwe
{
    public class Game1 : Game
    {
        private GraphicsDeviceManager _graphics;
        private SpriteBatch _spriteBatch;

        private int width = 800;
        private int height = 600;

        private Dictionary<ViewModel.VMType, Texture2D> bugTextures;
        private Dictionary<string, Texture2D> hexagonTextures;
        private Dictionary<int, Texture2D> gameoverTextures;
        private Texture2D glowTexture;
        private Texture2D backgroundTexture;

        private ViewModel viewModel;

        private RenderTarget2D wholeScreen;
        private RenderTarget2D leftSide;
        private RenderTarget2D rightSide;
        private RenderTarget2D middle;

        private VMField selectedVMField;
        private Model.Bug selectedBug;
        private List<Pos> possiblePlaceList;
        private int gameOver;

        ButtonState prevState;

        private RenderTarget2D debugScreen;
        private string debugText;

        private SpriteFont font;
        private SpriteFont hexfont;

        public Game1()
        {
            //first change in code
            _graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            IsMouseVisible = true;


            viewModel = new ViewModel();
        }

        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
            //_graphics.PreferredBackBufferWidth = 1200;
            _graphics.PreferredBackBufferWidth = 800;
            _graphics.PreferredBackBufferHeight = 600;
            _graphics.ApplyChanges();

            font = Content.Load<SpriteFont>("defaultfont");
            hexfont = Content.Load<SpriteFont>("hexfont");
            wholeScreen = new RenderTarget2D(_graphics.GraphicsDevice, 800, 600);
            leftSide = new RenderTarget2D(_graphics.GraphicsDevice, 200, 600);
            rightSide = new RenderTarget2D(_graphics.GraphicsDevice, 200, 600);
            middle = new RenderTarget2D(_graphics.GraphicsDevice, 400, 600);

            selectedVMField = null;
            possiblePlaceList = new List<Pos>();
            gameOver = 0;

            prevState = ButtonState.Released;


            debugScreen = new RenderTarget2D(_graphics.GraphicsDevice, 400, 600);
            debugText = "debugstart";

            viewModel = new ViewModel();
        }

        protected override void LoadContent()
        {
            _spriteBatch = new SpriteBatch(GraphicsDevice);

            bugTextures = new Dictionary<ViewModel.VMType, Texture2D>();
            bugTextures.Add(ViewModel.VMType.ANT, this.Content.Load<Texture2D>("ant"));
            bugTextures.Add(ViewModel.VMType.GRASSHOPPER, this.Content.Load<Texture2D>("grasshopper"));
            bugTextures.Add(ViewModel.VMType.QUEEN, this.Content.Load<Texture2D>("queen"));
            bugTextures.Add(ViewModel.VMType.SPIDER, this.Content.Load<Texture2D>("spider"));
            bugTextures.Add(ViewModel.VMType.EMPTY, this.Content.Load<Texture2D>("empty"));
            bugTextures.Add(ViewModel.VMType.FAKE, this.Content.Load<Texture2D>("empty"));

            hexagonTextures = new Dictionary<string, Texture2D>();
            hexagonTextures.Add("red", this.Content.Load<Texture2D>("hexagon_red"));
            hexagonTextures.Add("blue", this.Content.Load<Texture2D>("hexagon_blue"));
            hexagonTextures.Add("gray", this.Content.Load<Texture2D>("hexagon_gray"));
            hexagonTextures.Add("empty", this.Content.Load<Texture2D>("empty"));

            gameoverTextures = new Dictionary<int, Texture2D>();
            gameoverTextures.Add(1, this.Content.Load<Texture2D>("redwon"));
            gameoverTextures.Add(2, this.Content.Load<Texture2D>("bluewon"));

            glowTexture = this.Content.Load<Texture2D>("glow");

            backgroundTexture = this.Content.Load<Texture2D>("background");
        }

        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();


            // TODO: Add your update logic here
            var mouseState = Mouse.GetState();

            if(mouseState.RightButton == ButtonState.Pressed)
            {
                /*
                debugText = "Selected field:\n";
                int mouseX = mouseState.X - 200; //relative to renderTarget
                int mouseY = mouseState.Y;

                var s = viewModel.VMFieldOn(mouseX, mouseY);
                if (s is null)
                {
                    debugText += "null";
                }
                else
                {
                    debugText += s.modelField.Pos.ToString() + "\n";
                }
                gameOver = 2;
                */
                /*
                debugText += "selected bug: ";
                if (selectedBug is null)
                    debugText += "null\n";
                else
                    debugText += selectedBug.Typ;
                */
                /*
                viewModel.getSideVMfields();
                debugText += "allbugs:\n";
                foreach(var t in Bug.GetAllBugs())
                {
                    debugText += t.ToString() + "\n";
                }
                */

                
                debugText += "vmfields:\n";
                foreach(var vmf in viewModel.vmfields){
                    debugText += vmf.ToString() + "\n";
                }
                

            }

            if(prevState == ButtonState.Released && mouseState.LeftButton == ButtonState.Pressed)
            {
                if (gameOver > 0)
                {
                    gameOver = 0;
                    viewModel.model.NewGame();
                    return;
                }



                int mouseX = mouseState.X; //relative to renderTarget
                int mouseY = mouseState.Y;
                bool clickedOnMiddle = 200 <= mouseX && mouseX <= 600;

                if (clickedOnMiddle)
                    mouseX -= 200;

                selectedVMField = viewModel.VMFieldOn(mouseX, mouseY, !clickedOnMiddle); //hand=not clickedOnMiddle
                if (selectedVMField is null)
                {
                    prevState = mouseState.LeftButton;
                    return;
                }

                var selectableVMFieldsWithBug = new List<VMField>();


                foreach (var vmf in viewModel.vmfields)
                {
                    //TODO if ugyanaz a szín stb.
                    if (!(vmf.bug is null)) // ha van rajta bug (hand vagy nemüres nemfake onboard field)
                    {
                        selectableVMFieldsWithBug.Add(vmf);
                    }
                }


                //ha bogaras mezőre kattint, akkor legyen kiválasztva az a bogár
                if (selectableVMFieldsWithBug.Contains(selectedVMField))
                {
                    /*
                    //ha arra kattintok, ami már eleve ki van választva
                    if ((!(selectedBug is null)) && (!(selectedVMField is null)) && selectedVMField.bug == selectedBug)
                    {
                        //törli a kiválasztást
                        selectedBug = null;
                        debugText += "TOROLTEM!!!!!!\n";
                        possiblePlaceList.Clear();
                        return;
                    }
                    */
                    selectedBug = selectedVMField.bug;

                    possiblePlaceList.Clear();
                    
                    possiblePlaceList = viewModel.model.GetPossiblePlaceList(selectedBug);
                    //return;
                }

                //ha már ki van választva egy bogár
                //és egy érvényes helyre lép
                else if ((!(selectedBug is null)) && possiblePlaceList.Contains(selectedVMField.modelField.Pos))
                {
                    //TODO model.placeBug
                    viewModel.model.PlaceBug(selectedVMField.modelField.Pos, selectedBug);
                    debugText += "\nlerakom ezt:\n";
                    debugText += selectedBug;
                    debugText += "\nide:";
                    debugText += selectedVMField.modelField.Pos.ToString() + "\n";

                    selectedBug = null;
                    selectedVMField = null;
                    possiblePlaceList.Clear();
                }

                gameOver = viewModel.model.GetWinner();
                debugText += "vege:\n";
            }



            prevState = mouseState.LeftButton;
            base.Update(gameTime);
        }

        private void DrawField(string color, ViewModel.VMType bugType, int x, int y, int width, int height, bool isGlowing, string text="")
        {
            _spriteBatch.Draw(hexagonTextures[color], new Rectangle(x, y, width, height), Color.White);
            _spriteBatch.Draw(bugTextures[bugType], new Rectangle(x, y, width, height), Color.White);
            if (isGlowing)
                _spriteBatch.Draw(glowTexture, new Rectangle(x, y, width, height), Color.White);

            //DEBUG
            _spriteBatch.DrawString(font, text, new Vector2(x+width/2, y+height/2), Color.Black);
        }

        private void DrawVMField(VMField vmf, bool rightSide=false)
        {
            
            int iSize = Convert.ToInt32(vmf.rectSize);
            int ix = Convert.ToInt32(vmf.rectX);
            int iy = Convert.ToInt32(vmf.rectY);
            bool isGlowing = (!(vmf.modelField is null)) && possiblePlaceList.Contains(vmf.modelField.Pos);

            if (rightSide)
            {
                ix -= 200 + 400;
            }

            //DEBUG
            string text = "";
            //text += vmf.modelField is null ? "" : vmf.modelField.Pos.ToString();


            DrawField(vmf.color, vmf.type, ix, iy, iSize, iSize, isGlowing, text);
        }

        private void DrawBackground()
        {
            GraphicsDevice.SetRenderTarget(wholeScreen);
            GraphicsDevice.Clear(Color.White);
            _spriteBatch.Begin();
            _spriteBatch.Draw(backgroundTexture, new Rectangle(0, 0, 800, 600), Color.White);
            _spriteBatch.End();
            GraphicsDevice.SetRenderTarget(null);
        }

        private void DrawMiddle()
        {
            GraphicsDevice.SetRenderTarget(middle);
            GraphicsDevice.Clear(Color.White*.0f);
            _spriteBatch.Begin();

            viewModel.getVMfields();
            foreach(var f in viewModel.vmfields)
            {
                if(! f.isHand)
                    DrawVMField(f);
            }

            _spriteBatch.End();
            GraphicsDevice.SetRenderTarget(null);
        }

        private void DrawDebug()
        {
            GraphicsDevice.SetRenderTarget(debugScreen);
            GraphicsDevice.Clear(Color.White);
            _spriteBatch.Begin();

            _spriteBatch.DrawString(font, debugText, new Vector2(50, 50), Color.Black);

            _spriteBatch.End();
            GraphicsDevice.SetRenderTarget(null);
        }

        private void DrawSides()
        {
            //left side
            GraphicsDevice.SetRenderTarget(leftSide);
            GraphicsDevice.Clear(Color.White*.0f);
            _spriteBatch.Begin();

            viewModel.getSideVMfields();
            foreach(var f in viewModel.sideVmfields)
            {
                if(f.bug.Player.Id == 1)
                    DrawVMField(f);
            }

            int i = 0;
            foreach (Bug.Type t in Enum.GetValues(typeof(Bug.Type)))
            {
                _spriteBatch.DrawString(hexfont, viewModel.player1bugs[t].ToString(), new Vector2(120, i * 150), Color.Black);
                i++;
            }
            _spriteBatch.End();

            //right side
            GraphicsDevice.SetRenderTarget(rightSide);
            GraphicsDevice.Clear(Color.White * .0f);
            _spriteBatch.Begin();

            foreach (var f in viewModel.sideVmfields)
            {
                if (f.bug.Player.Id == 2)
                    DrawVMField(f, true);
            }

            i = 0;
            foreach (Bug.Type t in Enum.GetValues(typeof(Bug.Type)))
            {
                _spriteBatch.DrawString(hexfont, viewModel.player2bugs[t].ToString(), new Vector2(120, i * 150), Color.Black);
                //_spriteBatch.DrawString(hexfont, )
                i++;
            }
            _spriteBatch.End();

            GraphicsDevice.SetRenderTarget(null);
        }


        private void DrawGameOver()
        {
            if (gameOver == 0)
                return;

            _spriteBatch.Begin();

            _spriteBatch.Draw(gameoverTextures[gameOver], new Rectangle(0, 0, 800, 600), Color.White*0.4f);

            _spriteBatch.End();
            GraphicsDevice.SetRenderTarget(null);
        }
        protected override void Draw(GameTime gameTime)
        {
            DrawBackground();
            DrawSides();
            DrawMiddle();

            DrawDebug();

            GraphicsDevice.Clear(Color.Black);
            _spriteBatch.Begin();
            _spriteBatch.Draw(backgroundTexture, new Rectangle(0, 0, 800, 600), Color.White);
            _spriteBatch.Draw(leftSide, new Rectangle(0, 0, 200, 600), Color.White);
            _spriteBatch.Draw(middle, new Rectangle(200, 0, 400, 600), Color.White);
            _spriteBatch.Draw(rightSide, new Rectangle(600, 0, 200, 600), Color.White);
            

            _spriteBatch.Draw(debugScreen, new Rectangle(800, 0, 400, 600), Color.White);

            _spriteBatch.End();

            DrawGameOver();

            base.Draw(gameTime);
        }
    }
}
