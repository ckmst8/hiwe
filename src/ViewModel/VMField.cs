﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace Hiwe
{
    class VMField
    {
        public double rectX, rectY, rectSize;
        public string color;
        public ViewModel.VMType type;
        public Model.Field modelField;
        public Model.Bug bug;
        public bool isReal;
        public bool isHand;

        public double internalRectX, internalRectY, internalRectWidth, internalRectHeight;
        public double topleftTriX, topleftTriY, topleftTriWidth, topleftTriHeight;
        public double toprightTriX, toprightTriY, toprightTriWidth, toprightTriHeight;
        public double bottomleftTriX, bottomleftTriY, bottomleftTriWidth, bottomleftTriHeight;
        public double bottomrightTriX, bottomrightTriY, bottomrightTriWidth, bottomrightTriHeight;

        public VMField(int rectX_, int rectY_, int rectSize_, ViewModel.VMType type_, string color_ = "white", Model.Field modelField_=null) :
            this(Convert.ToDouble(rectX_), Convert.ToDouble(rectY_), Convert.ToDouble(rectSize_), type_, color_, modelField_)
        { 

        }

        public VMField(double rectX_, double rectY_, double rectSize_, ViewModel.VMType type_, string color_ = "white", Model.Field modelField_ = null)
        {
            rectX = rectX_;
            rectY = rectY_;
            rectSize = rectSize_;
            color = color_;
            type = type_;
            modelField = modelField_;
            bug = null;
            if ((!(modelField is null)) && modelField.HasBug)
                bug = modelField.BugTop;

            isReal = (!(modelField is null));
                
            isHand = (!isReal) && new[]{ViewModel.VMType.ANT,
                    ViewModel.VMType.QUEEN,
                    ViewModel.VMType.GRASSHOPPER,
                    ViewModel.VMType.SPIDER}.Contains(type);

            internalRectX = rectX + rectSize / 3;
            internalRectY = rectY;
            internalRectWidth = rectSize / 3;
            internalRectHeight = rectSize;


            topleftTriX = rectX;
            topleftTriY = rectY;
            topleftTriWidth = rectSize / 3;
            topleftTriWidth = rectSize / 2;

            toprightTriX = rectX + (2/3)*rectSize;
            toprightTriY = rectY;
            toprightTriWidth = rectSize / 3;
            toprightTriWidth = rectSize / 2;

            bottomleftTriX = rectX;
            bottomleftTriY = rectY + rectSize / 2;
            bottomleftTriWidth = rectSize / 3;
            bottomleftTriWidth = rectSize / 2;

            bottomrightTriX = rectX + (2/3)*rectSize;
            bottomrightTriY = rectY + rectSize / 2;
            bottomrightTriWidth = rectSize / 3;
            bottomrightTriWidth = rectSize / 2;
        }

        public bool containsPx(int ix, int iy)
        {
            double x = Convert.ToDouble(ix);
            double y = Convert.ToDouble(iy);

            if (pointInsideRect(x, y, internalRectX, internalRectY, internalRectWidth, internalRectHeight))
                return true;


            // take the remainder part of the hexagon: it's 4 right-angled triangle
            // above or below y=mx+b diagonal line ?
            // b=y-mx for leftmostX, leftmostY

            //topleft
            else if (pointInsideRect(x, y, topleftTriX, topleftTriY, topleftTriWidth, topleftTriHeight))
            {
                double leftmostX = topleftTriX;
                double leftmostY = topleftTriY + topleftTriHeight;
                double m = 3 / 2;
                double b = leftmostY - m*leftmostX;

                //below diagonal
                return y < m * x + b;
            }
            //topright
            else if (pointInsideRect(x,y, toprightTriX, toprightTriY, toprightTriWidth, toprightTriHeight))
            {
                double leftmostX = toprightTriX;
                double leftmostY = toprightTriY;
                double m = -3 / 2;
                double b = leftmostY - m * leftmostX;

                //below diagonal
                return y < m * x + b;
            }
            //bottomleft
            else if (pointInsideRect(x, y, bottomleftTriX, bottomleftTriY, bottomleftTriWidth, bottomleftTriHeight))
            {
                double leftmostX = bottomleftTriX;
                double leftmostY = bottomrightTriX;
                double m = -3 / 2;
                double b = leftmostY - m * leftmostX;

                //above diagonal
                return y > m * x + b;
            }
            //bottomright
            else if (pointInsideRect(x, y, bottomrightTriX, bottomrightTriY, bottomrightTriWidth, bottomrightTriHeight))
            {
                double leftmostX = bottomrightTriX;
                double leftmostY = bottomrightTriY + bottomrightTriHeight;
                double m = 3 / 2;
                double b = leftmostY - m * leftmostX;

                //above diagonal
                return y > m * x + b;
            }


            return false;
        }

        private bool pointInsideRect(double x, double y, double rectangleX, double rectangleY, double width, double height)
        {
            if (rectangleX < x && x < rectangleX + width && rectangleY < y && y < rectangleY+ height)
                return true;
            return false;
        }

        public override string ToString()
        {
            string ret = "---\n";
            ret += String.Format("x={0} y={1} size={2}\n", rectX, rectY, rectSize);
            ret += String.Format("bug={0} hand={1}\n", bug, isHand);
            return ret;
        }
    }
}
