﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;
using Hiwe.Model;

namespace Hiwe
{
    class ViewModel
    {

        public Model.Model model;
        public enum VMType {QUEEN, ANT, SPIDER, GRASSHOPPER, EMPTY, FAKE};
        public Dictionary<Bug.Type, int> player1bugs;
        public Dictionary<Bug.Type, int> player2bugs;
        public List<VMField> vmfields;
        public List<VMField> sideVmfields;

        public ViewModel()
        {
            player1bugs = new Dictionary<Bug.Type, int>();
            player2bugs = new Dictionary<Bug.Type, int>();

            model = Model.Model.instance();
            vmfields = new List<VMField>();
            sideVmfields = new List<VMField>();

            //Bug[] bugs = model.Player1Bugs.ToArray();
            //var q = Bug.Create(Bug.Type.QUEEN, model.Player1);
            //var a = Bug.Create(Bug.Type.ANT, model.Player1);
            
            //model.PlaceBug(new Pos(0,0), q);
            //model.PlaceBug(new Pos(1, 0), a);
            //model.PlaceBug(new Pos(-1, 0), q);
        }

        public void loadPlayerBugs()
        {
            foreach(Bug.Type t in Enum.GetValues(typeof(Bug.Type)))
            {
                player1bugs[t] = model.Player1.GetHandBugListByType(t).Count;
                player2bugs[t] = model.Player2.GetHandBugListByType(t).Count;
            }
        }

        public void getSideVMfields()
        {
            loadPlayerBugs();
            int i;

            i = 0;
            foreach (Bug.Type t in Enum.GetValues(typeof(Bug.Type)))
            {
                var p1hand = model.Player1.GetHandBugListByType(t);
                if(p1hand.Count > 0)
                {
                    VMField sidevmfield = new VMField(0, i * 150, 100, bugType2VMType(t), "red");
                    sidevmfield.bug = p1hand[p1hand.Count - 1];
                    sideVmfields.Add(sidevmfield);
                    vmfields.Add(sidevmfield);
                }
                i++;
            }

            i = 0;
            foreach (Bug.Type t in Enum.GetValues(typeof(Bug.Type)))
            {
                var p2hand = model.Player2.GetHandBugListByType(t);
                if (p2hand.Count > 0)
                {
                    VMField sidevmfield = new VMField(0 + 200 + 400, i * 150, 100, bugType2VMType(t), "blue");
                    sidevmfield.bug = p2hand[p2hand.Count - 1];
                    sideVmfields.Add(sidevmfield);
                    vmfields.Add(sidevmfield);
                }
                i++;
            }
        }

        //TODO 400 és 600 helyett változók
        public void getVMfields()
        {
            vmfields.Clear();
            getSideVMfields();
            
            var mtx = getBoardMatrix();

            int mtxWidth = mtx.GetLength(0);
            int mtxHeight = mtx.GetLength(1);

            double realWidth = 6 * mtxWidth + 3 * (mtxHeight % 2) + 1;
            double realHeight = 2 * mtxHeight + 1;


            double fieldSize, downShift, rightShift;
            //if we expand the according to width
            if (realWidth / 400 > realHeight / 600)
            {
                fieldSize = 400 / (realWidth + 1);
                rightShift = 0;
                downShift = 600 / 2 - fieldSize * realHeight / 2;
            }
            //according to height
            else
            {
                fieldSize = 600 / (realHeight + 1);
                downShift = 0;
                rightShift = 600 / 2 - fieldSize * realHeight / 2;
            }

            int iFieldSize = Convert.ToInt32(fieldSize);
            int iRightShift = Convert.ToInt32(rightShift);
            int iDownShift = Convert.ToInt32(downShift);


            VMField act;
            for (int i = 0; i < mtxWidth; i++)
            {
                for (int j = 0; j < mtxHeight; j++)
                {
                    var modelField = mtx[i, j];
                    ViewModel.VMType t;

                    if (modelField is null)
                        t = VMType.FAKE;
                    else if(! modelField.HasBug)
                        t = VMType.EMPTY;
                    else
                        t = bugType2VMType(modelField.BugTop.Typ);


                    string fieldColor;
                    if (t == ViewModel.VMType.EMPTY)
                        fieldColor = "gray";
                    else if (t == ViewModel.VMType.FAKE)
                        fieldColor = "empty";
                    else
                    {
                        int playerId = modelField.BugTop.Player.Id;
                        if (playerId == 1)
                            fieldColor = "red";
                        else
                            fieldColor = "blue";
                    }
                        

                    int x = 6 * i + 3 * (j % 2);
                    int y = 2 * j;

                    act = new VMField(x * iFieldSize + iRightShift,
                        y * iFieldSize + iDownShift,
                        4 * iFieldSize,
                        t,
                        fieldColor,
                        modelField);
                    vmfields.Add(act);
                }
            }
        }

        public Field[,] getBoardMatrix()
        {
            int minRow = Int32.MaxValue;
            int minCol = Int32.MaxValue;

            int maxRow = Int32.MinValue;
            int maxCol = Int32.MinValue;

            foreach (var field in model.Board.Fields)
            {
                if (field.Pos.Row < minRow)
                    minRow = field.Pos.Row;
                if (field.Pos.Col < minCol)
                    minCol = field.Pos.Col;

                if (field.Pos.Row > maxRow)
                    maxRow = field.Pos.Row;
                if (field.Pos.Col > maxCol)
                    maxCol = field.Pos.Col;
            }

            /*Debug.WriteLine("kezdeti:");
            Debug.WriteLine("minCol=" + minCol.ToString());
            Debug.WriteLine("maxCol=" + maxCol.ToString());
            Debug.WriteLine("minRow=" + minRow.ToString());
            Debug.WriteLine("maxRow=" + maxRow.ToString());*/

            //top row index should be either 0 or 1
            int downShift = 0;
            while (minRow > 1)
            {
                downShift -= 2;
                minRow -= 2;
                maxRow -= 2;
            }
            while(minRow < 0)
            {
                downShift += 2;
                minRow += 2;
                maxRow += 2;
            }

            //field at top column should have a col index 0 AND an even row index
            int rightShift = 0;
            while(minCol > 0)
            {
                rightShift -= 1;
                minCol -= 1;
                maxCol -= 1;
            }
            while (minCol < 0)
            {
                rightShift += 1;
                minCol += 1;
                maxCol += 1;
            }

            /*Debug.WriteLine("shift utáni:");
            Debug.WriteLine("minCol=" + minCol.ToString());
            Debug.WriteLine("maxCol=" + maxCol.ToString());
            Debug.WriteLine("minRow=" + minRow.ToString());
            Debug.WriteLine("maxRow=" + maxRow.ToString());
            Debug.WriteLine("downShift=" + downShift.ToString());
            Debug.WriteLine("rightShift=" + rightShift.ToString());*/


            //Model.Field[,] mtx = new Model.Field[maxY - minY + 1, maxX - minX + 1];
            //Field[,] mtx = new Field[maxRow+1, maxCol+1];
            Field[,] mtx = new Field[maxCol+1, maxRow+ 1];

            for (int i = 0; i <= maxRow; i++)
            {
                for (int j = 0; j <= maxCol; j++)
                {
                    bool realbug = false;
                    bool realfield = false;
                    Pos modelFieldPos = new Pos(new Pos(i - downShift, j - rightShift));
                    /*Debug.WriteLine("i=" + i.ToString() + " j=" + j.ToString() + " modelFieldPos=" + modelFieldPos.ToString());*/
                    var field = model.Board.GetField(modelFieldPos);
                    if (model.Board.HasField(modelFieldPos))
                    {
                        if (field != null)
                        {
                            realfield = true;
                            var topBug = field.BugTop;
                            if (topBug != null)
                            {
                                mtx[j, i] = field;
                                //mtx[i,j] = field;
                                realbug = true;
                            }
                        }
                    }

                    if (!realfield)
                        mtx[j, i] = null;
                        //mtx[i, j] = null;
                    else if (!realbug)
                        mtx[j, i] = field;
                        //mtx[i, j] = field;
                    
                }
            }

            return mtx;
        }

        public VMField VMFieldOn(int x, int y, bool hand=false)
        {
            foreach(var f in vmfields)
            {
                if (f.isHand == hand && f.containsPx(x, y))
                    return f;
            }
            return null;
        }

        public VMType bugType2VMType(Bug.Type bt)
        {
            switch (bt)
            {
                case Bug.Type.ANT:
                    return VMType.ANT;
                case Bug.Type.GRASSHOPPER:
                    return VMType.GRASSHOPPER;
                case Bug.Type.QUEEN:
                    return VMType.QUEEN;
                case Bug.Type.SPIDER:
                    return VMType.SPIDER;
                default:
                    return VMType.EMPTY;
            }
        }
    }
}
