﻿using System;
using System.Collections.Generic;
using System.Text;
using Hiwe.Model;
using Xunit;

namespace test
{
    public class TestPos
    {
        [Fact]
        public void Equality()
        {
            var p1 = new Pos(1, 2);
            var p1also = new Pos(1, 2);
            var p2 = new Pos(2, 1);
            
            Assert.True(p1 == p1also);
            Assert.False(p1 != p1also);
            Assert.False(p1 == p2);
            Assert.True(p1 != p2);
        }

        [Fact]
        public void Hashcodes()
        {
            var p1 = new Pos(1, 2);
            var p1also = new Pos(1, 2);

            Assert.True(p1.GetHashCode() == p1also.GetHashCode());
        }

        [Fact]
        public void Directions()
        {
            var p = new Pos(2, 1);
            Assert.True(p.PosT == (new Pos(0, 1)));
            Assert.True(p.PosTL == (new Pos(1, 0)));
            Assert.True(p.PosTR == (new Pos(1, 1)));
            Assert.True(p.PosB == (new Pos(4, 1)));
            Assert.True(p.PosBL == (new Pos(3, 0)));
            Assert.True(p.PosBR == (new Pos(3, 1)));
        }

        [Fact]
        public void Relations()
        {
            var p = new Pos(2, 1);
            Assert.Equal(p.Relation(p.PosT), Pos.DIR_T);
            Assert.Equal(p.Relation(p.PosTL), Pos.DIR_TL);
            Assert.Equal(p.Relation(p.PosTR), Pos.DIR_TR);
            Assert.Equal(p.Relation(p.PosB), Pos.DIR_B);
            Assert.Equal(p.Relation(p.PosBL), Pos.DIR_BL);
            Assert.Equal(p.Relation(p.PosBR), Pos.DIR_BR);
            Assert.Equal(p.Relation(p.PosBR.PosBR), -1);
        }

    }
}
