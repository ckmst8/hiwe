﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using Hiwe.Model;
using Xunit;

namespace test
{
    public class TestBoard
    {
        [Fact]
        public void Clear()
        {
            var brd = Board.instance();
            brd.PlaceFieldL(new Pos(0, 0));
            brd.PlaceFieldL(new Pos(1, 1));
            brd.Clear();
            Assert.Equal(brd.FieldsNum, 0);
        }

        [Fact]
        public void PlaceField()
        {
            var brd = Board.instance();
            brd.Clear();

            //Test PlaceFieldA
            Field[] fArray = brd.PlaceFieldA(new Pos(0, 0));
            Field[] fArraySol = { new Field(new Pos(-1, -1)), new Field(new Pos(-2, 0)),
                  new Field(new Pos(-1, 0)), new Field(new Pos(1, 0)), new Field(new Pos(2, 0)),
                  new Field(new Pos(1, -1)), new Field(new Pos(0, 0)) };
            Assert.Equal(fArraySol, fArray);

            //Test PlaceFieldL
            List<Field> fList = brd.PlaceFieldL(new Pos(1, 0));
            List<Field> fListSol = new List<Field> {
                new Field(new Pos(1, 0)),
                new Field(new Pos(0, 1)),
                new Field(new Pos(3, 0)),
                new Field(new Pos(2, 1)),
            };
            Assert.Equal(fListSol, fList);
        }
    }
}
 